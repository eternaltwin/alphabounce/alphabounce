import haxe.Unserializer;
import haxe.crypto.BaseCode;

typedef _Travel = {
	public var _name:String;
	public var _sx:Int;
	public var _sy:Int;
	public var _ex:Int;
	public var _ey:Int;
	public var _start:Int;
	public var _dest:Int;
}

enum _ServerRequest {
	_AskInfos;
	_Play(x:Int, y:Int);
	_EndGame(x:Int, y:Int, victory:Bool, mineral:Int, missiles:Int, itemId:Int, shopItems:Array<Int>);
	_BuyItem(itemId:Int);
	_SubmitLevel(str:String);

	_PlayLander(x:Int, y:Int);
	_EndLander(victory:Bool, mineral:Int, missiles:Int, caps:Int, itemId:Int, travel:_Travel, flMarkHouse:Bool);
	_AskPendingLevels(x:Int, y:Int);
	_SelectPendingLevel(x:Int, y:Int, id:Int);
	_DeletePendingLevels(x:Int, y:Int);
	_ResetLevel(x:Int, y:Int);
	_EndStory(choice:Int, itemId:Int);
	_Wrap;
}

enum _ServerResponse {
	_Confirm();
	_ConfirmMove(x:Int, y:Int, hasMineral:Bool, levelData:String);
	_ConfirmLander(hasMineral:Bool, capsType:Int, visited:Bool);
	_Error(str:String);
	_SetInfos(str:String, key:String, knb:Int);
	_PendingLevels(a:Array<String>);
}

typedef _PlayerData = {
	public var _flAdmin:Bool;
	public var _flEditor:Bool;
	public var _pid:Int;
	public var _ox:Int;
	public var _oy:Int;
	public var _x:Int;
	public var _y:Int;
	public var _chl:Int; // liquid hydrogen (temporary)
	public var _chs:Int; // solid hydrogen (persistant)
	public var _minerai:Int;
	public var _missions:Array<Int>;
	public var _missile:Int;
	public var _missileMax:Int;
	public var _plays:Int;
	public var _engine:Int;
	public var _radar:Int;
	public var _life:Int;
	public var _drone:Int;
	public var _items:Array<Int>;
	public var _shopItems:Array<Int>;
	public var _fog:Array<Int>;
	public var _comp:Array<Int>;
	public var _square:Array<Int>;
	// *_* NEW *_*
	public var _travel:Array<_Travel>;
	public var _pendingLevels:Int; // nombres de niveaux en attente ( -1 si niveau deja validé );
}

class Call {
	static var url = "https://www.alphabounce.com/play/intercom";
	static var BASE = "_/abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
	static var knb = 2;
	static var key = "ItztXg3JvAgNOqnIMP2y54QtS9ZuZRG6";

	static function main() {
		var sr:_ServerRequest = _AskInfos;

		var request = new haxe.Http(url);
		request.onData = (data) -> {
			trace(data);
			var resp = data.substr('<r>'.length, data.length - 7);
			var response:_ServerResponse = Unserializer.run(decode(resp));
			switch (response) {
				case _SetInfos(str, key, knb):
					trace(str);
					Call.key = key;
					Call.knb = knb;
				default:
					trace(Std.string(response));
			}
		};
		request.addHeader('Cookie', 'sid=c5eEP3aYujzJjnVfUZPRTpV9TdIgSE31');
		request.setParameter("knb", Std.string(knb));
		request.setParameter("cmd", encode(haxe.Serializer.run(sr)));
		request.request(false);
	}

	static function encode(str:String) {
		if (key == null)
			return str;
		str = BaseCode.encode(str, BASE);
		return new Codec(key).encode(str);
	}

	static function decode(str:String) {
		if (key == null)
			return str;
		str = new Codec(key).decode(str);
		return BaseCode.decode(str, BASE);
	}
}

class Codec {
	static inline var IDCHARS = "$uasxIIntfo";
	public static var BASE64 = ":_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

	var str:String;
	var key:String;
	var ikey:Int;
	var pkey:Int;
	var crc:Int;

	public function new(key:String) {
		this.key = key;
		ikey = 0;
		for (i in 0...key.length) {
			ikey *= 51;
			ikey += key.charCodeAt(i);
			ikey = untyped ikey & 0xFFFFFFFF;
		}
	}

	function makeCrcString() {
		return BASE64.charAt(crc & 63) + BASE64.charAt((crc >> 6) & 63) + BASE64.charAt((crc >> 12) & 63) + BASE64.charAt((crc >> 18) & 63)
			+ BASE64.charAt((crc >> 24) & 63);
	}

	public function encode(o:Dynamic):String {
		pkey = 0;
		crc = 0;
		str = "";
		encodeAny(o);
		str += makeCrcString();
		return str;
	}

	public function decode(s:String):Dynamic {
		pkey = 0;
		crc = 0;
		str = s;
		var o = decodeAny();
		var crc = makeCrcString();
		if (str != crc)
			throw "Decode CRC error : " + str + " != " + crc + " (o=" + o + ")";
		return o;
	}

	function writeStr(s:String) {
		for (i in 0...s.length)
			writeChar(s.charAt(i));
	}

	function writeChar(c) {
		var p = BASE64.indexOf(c, 0);
		if (p == -1) {
			str += c;
			return;
		} else {
			var out = p ^ ((ikey >> pkey) & 63);
			crc *= 51;
			crc += out;
			crc = crc ^ 0xFFFFFFFF;
			str += BASE64.charAt(out);
		}
		pkey += 6;
		if (pkey >= 28)
			pkey -= 28;
	}

	function readChar() {
		var cc = str.charAt(0);
		var c = BASE64.indexOf(cc, 0);
		str = str.substr(1);
		if (c == -1)
			return cc;
		crc *= 51;
		crc += c;
		crc = crc ^ 0xFFFFFFFF;
		c = c ^ ((ikey >> pkey) & 63);
		pkey += 6;
		if (pkey >= 28)
			pkey -= 28;
		return BASE64.charAt(c);
	}

	function readStr() {
		var s = "";
		while (true) {
			var c = readChar();
			if (c == null)
				return null;
			if (c == ":")
				break;
			s += c;
		}
		return s;
	}

	function encodeArray(o:Array<Dynamic>) {
		writeStr(Std.string(o.length));
		writeStr(":");
		for (v in o)
			encodeAny(v);
	}

	function encodeObject(o:Dynamic) {
		for (f in Reflect.fields(o)) {
			writeStr(f);
			writeStr(":");
			encodeAny(Reflect.field(o, f));
		}
		writeStr(":");
	}

	function encodeAny(o:Dynamic) {
		if (o == null)
			writeStr(IDCHARS.charAt(1));
		else if (Std.is(o, Array)) {
			writeStr(IDCHARS.charAt(2));
			encodeArray(untyped o);
		} else
			switch (js.Lib.typeof(o)) {
				case "string":
					writeStr(IDCHARS.charAt(3));
					writeStr(untyped o);
					writeStr(":");
				case "number":
					var n:Int = untyped o;
					/*
						if( Std.isNaN(n) )
							writeStr( IDCHARS.charAt(4) );
						else if( n == downcast(Std).infinity )
							writeStr( IDCHARS.charAt(5) );
						else if( n == -downcast(Std).infinity )
							writeStr( IDCHARS.charAt(6) );
						else {
					 */
					// n = Int(n);
					writeStr(IDCHARS.charAt(7));
					if (n < 0)
						writeStr(IDCHARS.charAt(1));
					writeStr(Std.string(Math.abs(n)));
					writeStr(":");
				// }
				case "boolean":
					if (untyped o == true)
						writeStr(IDCHARS.charAt(8));
					else
						writeStr(IDCHARS.charAt(9));
				default:
					writeStr(IDCHARS.charAt(10));
					encodeObject(untyped o);
			}
	}

	function decodeArray() {
		var n = Std.parseInt(readStr());
		var a = new Array();
		for (i in 0...n)
			a.push(decodeAny());
		return a;
	}

	function decodeObject() {
		var o = {};
		while (true) {
			var k = readStr();
			if (k == null)
				return null;
			if (k == "")
				break;
			var v = decodeAny();
			Reflect.setField(o, k, v);
		}
		return o;
	}

	function decodeAny():Dynamic {
		var c = readChar();
		switch (c) {
			case 'u':
				return null;
			case 'a':
				return decodeArray();
			case 's':
				return readStr();
			//		case IDCHARS.charAt(4): return Std.cast(null * 1); // NaN
			//		case IDCHARS.charAt(5): return downcast(Std).infinity;
			//		case IDCHARS.charAt(6): return Std.cast(-downcast(Std).infinity);
			case 'n':
				var neg = readChar();
				var str = readStr();
				var n;
				if (neg == IDCHARS.charAt(1))
					n = -Std.parseInt(str);
				else
					n = Std.parseInt(neg + str);
				return n;
			case 't':
				return true;
			case 'f':
				return false;
			case 'o':
				return decodeObject();
			default:
				return null;
		}
	}
}
