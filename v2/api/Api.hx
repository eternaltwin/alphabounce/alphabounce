import haxe.crypto.Md5;
import js.node.Fs;
import haxe.ds.StringMap;
import js.lib.Promise;
import jsasync.IJSAsync;
import Protocol._ServerRequest;
import Protocol._ServerResponse;
import Protocol._LogData;
import haxe.Serializer;
import haxe.Unserializer;
import Express;

using StringTools;
using Std;
using jsasync.JSAsyncTools;

class Api implements IJSAsync {
	static inline private var MAX_PLAYER_UID = 0x7FFFFFFF;

	@:jsasync static function isDepleted(pi:PlayerInfo, x:Int, y:Int):Promise<Bool> {
		var levelInfos:StringMap<Int> = getLevelMap(pi.pid).jsawait();

		// minerals regenerate after 200 plays
		return levelInfos.get('$x,$y') >= pi.plays - 200;
	}

	@:jsasync static function getLevelMap(uid:Int):Promise<StringMap<Int>> {
		var key = 'alphabounce:level:user:$uid';
		var levelInfosR:String = RedisClient.get(key).jsawait();

		return if (levelInfosR == null) new StringMap() else Unserializer.run(levelInfosR);
	}

	@:jsasync static function generateFog(uid:Int, x:Int, y:Int):Promise<Array<Int>> {
		var levelInfos:StringMap<Int> = getLevelMap(uid).jsawait();

		var fog = [];
		for (realX in x - 10...x + 10) {
			for (realY in y - 10...y + 10) {
				var result:Int = null;
				var status = levelInfos.get('$realX,$realY');
				if (status > 0) result = 1;
				else if (status < 0) result = 0;
				fog.push(result);
			}
		}

		return fog;
	}

	@:jsasync static function setVictory(pi:PlayerInfo, x:Int, y:Int, victory:Bool):Promise<Dynamic> {
		var levelInfos:StringMap<Int> = getLevelMap(pi.pid).jsawait();

		if (levelInfos.get('$x,$y') > 0 || victory) levelInfos.set('$x,$y', pi.plays);
		else levelInfos.set('$x,$y',-pi.plays);

		var key = 'alphabounce:level:user:${pi.pid}';
		return RedisClient.set(key, Serializer.run(levelInfos)).jsawait();

		return null;
	}

	@:jsasync static function getUserInfos(uid:Int):Promise<PlayerInfo> {
		var key = 'alphabounce:user:$uid';
		var playerInfosR = RedisClient.get(key).jsawait();
		if (playerInfosR == null)
			return null;

		var pi = new PlayerInfo();
		pi.pid = uid;
		pi.parseInfo(playerInfosR);
		if (uid == 2138045094) {
			setPlayerPosition(pi, -36, -10);
			pi.addItem(MissionInfo.LANDER_REACTOR);
		}
		return pi;
	}

	@:jsasync static function setPlayerPosition(user:PlayerInfo, x:Int, y:Int):Promise<Dynamic> {
		user.ox = user.x;
		user.oy = user.y;
		user.x = x;
		user.y = y;
		writeUserInfos(user).jsawait();
		return null;
	}

	@:jsasync static function writeUserInfos(userInfos:PlayerInfo):Promise<Bool> {
		var key = 'alphabounce:user:${userInfos.pid}';
		return RedisClient.set(key, userInfos.exportInfos()).jsawait();
	}

	@:jsasync static function getEncodedUserInfos(playerInfos:PlayerInfo, ?uid:Int):Promise<String> {
		if (playerInfos == null) {
			var pi = new PlayerInfo();
			pi.setToDefault();
			pi.pid = uid;
			writeUserInfos(pi).jsawait();
			return getEncodedUserInfos(getUserInfos(uid).jsawait()).jsawait();
		} else {
			trace('${playerInfos.pid} -> ${playerInfos.x},${playerInfos.y}');

			if (playerInfos.pid == 112) {
				playerInfos.flAdmin = true;
			}

			playerInfos.fog = generateFog(playerInfos.pid, playerInfos.x, playerInfos.y).jsawait();

			return playerInfos.exportInfos();
		}
	}

	@:jsasync static function loadPlayer(req:ExpressRequest, res:ExpressResponse, next:?Dynamic->Void) {
		if (req.cookies.abuid == null) {
			req.locals = {};
			next();
			return;
		}
		var uid = req.cookies.abuid.parseInt();
		if (uid > MAX_PLAYER_UID) {
			req.locals = {};
			next();
			return;
		}

		req.locals = {};
		req.locals.uid = uid;
		req.locals.player = getUserInfos(uid).jsawait();
		if (req.locals.player.nick == null) req.locals.player.nick = req.locals.nick;
		next();
	}

	@:jsasync static function checkPlanetCompleted(player:PlayerInfo, x:Int, y:Int) {
		var planet = ZoneInfo.getPlanet(x, y);
		if (planet == null)
			return false;

		// We just cleared a planet case, let's see if we got all
		var finishedCases = getLevelMap(player.pid).jsawait();
		for (k in ZoneInfo.getSquares(planet)) {
			var x = k[0];
			var y = k[1];
			if (finishedCases.get('$x,$y') != 1) {
				trace('I haven\'t finished $x,$y.');
				return false;
			}
		}

		// We have everything, mark planet as completed!
		trace('Planet completed!');
		player.comp[planet] = 100;
		return true;
	}

	@:jsasync static function createLevelMission(faction:Int, rank:Int) {
		var ray = Math.random() * (rank + (faction == 1 ? 3 : 10)); // offsets need more rigorous confirming
		var angle = Math.random() * 2 * Math.PI;
		var size = faction == 1 ? 2 : 1; // TODO randomize size
		var m = {
			x: Math.round(Math.cos(angle) * ray - (size - 1) / 2),
			y: Math.round(Math.sin(angle) * ray - (size - 1) / 2),
			timestamp: Date.now().getTime(),
			size: size,
		};
		return m;
	}

	@:jsasync static function checkLevelMission(pi:PlayerInfo, x:Int, y:Int) {
		var l = pi.levelMission;
		if(x < l.x || x >= l.x + l.size || y < l.y || y >= l.y + l.size) return false;
		
		var levelInfosR:StringMap<Int> = getLevelMap(pi.pid).jsawait();
		for(sx in l.x...l.x + l.size) {
			for(sy in l.y...l.y + l.size) {
				if(!levelInfosR.exists('$sx,$sy') || levelInfosR.get('$sx,$sy') < 1) return false;
			}
		}
		
		return true;
	}

	@:jsasync static function intercom(req:ExpressRequest, res:ExpressResponse, next:?Dynamic->Void) {
		var uid = req.locals.uid;
		var player = req.locals.player;

		var body = req.body;
		var cmd:_ServerRequest = Unserializer.run(body.cmd);
		var response:Dynamic = switch (cmd) {
			case _AskInfos:
				// compatibility
				if (player.faction == null) {
					player.faction = 1;
					if (player.gotItem(86)) player.faction = 0;
					if (player.gotItem(94)) player.faction = 2;
					player.rank =0;
					writeUserInfos(player).jsawait();
				}
				player.tasks = [];
				_ServerResponse._SetInfos(getEncodedUserInfos(player, uid).jsawait(), null, 0);
			case _Play(x, y):
				trace('Start play: $x $y');
				var hasMineral = !isDepleted(player, x, y).jsawait();
				player.plays++;
				writeUserInfos(player).jsawait();
				_ServerResponse._ConfirmMove(x, y, hasMineral, null);
			case _BuyItem(id):
				var item = ShopInfo.ITEMS[id];
				if (item == null)
					return;
				if (player.minerai < item.price)
					return;

				player.tasks = [];
				player.buyShopItem(id);
				player.checkMission();
				writeUserInfos(player).jsawait();
				// to get popup data to the client, in case of a completed mission
				_Confirm(player.tasks.length > 0 ? player.tasks : null);
			case _EndGame(x, y, victory, mineral, missiles, itemId, shopItems):
				if (uid == 2138045094) {
					victory = true;
				}

				trace('End game $x $y: victory=$victory minerals=$mineral');
				setVictory(player, x, y, victory).jsawait();
				player.tasks = [];
				var changed = false;
				if (victory) {
					setPlayerPosition(player, x, y).jsawait();
					changed = checkPlanetCompleted(player, x, y).jsawait() || changed;
				}
				if (mineral > 0) {
					player.minerai += mineral;
					changed = true;
				}
				if (missiles != player.missile) {
					player.missile = missiles;
					changed = true;
				}
				if (itemId != null) {
					player.addItem(itemId);
					trace('collected item $itemId');
					changed = true;
				}
				changed = player.checkMission() || changed;
				if(player.levelMission != null && checkLevelMission(player, x, y).jsawait()) {
					var faction = player.faction;
					var r = faction == 1 ? (player.levelMission.size * player.levelMission.size * 10) : 75;
					player.minerai += r;
					player.rank++;
					var t:_LogData = { // for log and popup
						_id: faction == 1 ? 1000 : 1001,
						_status: 1,
						_timestamp: Date.now().getTime(),
						_data: [
							"x" => Std.string(player.levelMission.x),
							"y" => Std.string(player.levelMission.y),
							"size" => Std.string(player.levelMission.size),
							"reward" => Std.string(r),
						],
					}
					player.tasks.push(t);
					player.missionLog.push(t);
					player.levelMission = null;
					changed = true;
				}
				if(player.levelMission == null && player.faction > 0 && player.missions[1].status == 1) {
					player.levelMission = createLevelMission(player.faction, player.rank).jsawait();
					var m = player.levelMission;
					var t:_LogData = { // for the log
						_id: player.faction == 1 ? 1000 : 1001,
						_status: 0,
						_timestamp: m.timestamp,
						_data: [
							"reward" => Std.string(player.faction == 1 ? (m.size * m.size * 10) : 75),
							"xmin" => Std.string(m.x),
							"ymin" => Std.string(m.y),
						],
					};
					if (player.faction == 1) {
						t._data.set("xmax", Std.string(m.x + m.size - 1));
						t._data.set("ymax", Std.string(m.y + m.size - 1));
					}
					player.missionLog.push(t);
					changed = true;
				}
				
				if (changed) {
					// Persist & reload
					writeUserInfos(player).jsawait();
					player = getUserInfos(uid).jsawait();
				}

				_ServerResponse._SetInfos(getEncodedUserInfos(player).jsawait(), null, 0);
			default:
				"?";
		}
		res.end('<r>${Serializer.run(response)}</r>');
	}

	static var index_tpl:Null<String> = null;

	@:jsasync static function logout(req:ExpressRequest, res:ExpressResponse, next:?Dynamic->Void) {
		res.set({'set-cookie': 'abuid=; expires=Thu, 01 Jan 1970 00:00:00 GMT'});
		res.redirect("/");
	}

	@:jsasync static function registerPlayer(req:ExpressRequest, res:ExpressResponse, next:?Dynamic->Void) {
		var body = req.body;
		var uid = ("0x" + Md5.encode(body.nick).substr(0, 8)).parseInt();
		if (uid > MAX_PLAYER_UID) { // Prevent overflow on mt.persistcodec
			uid = ("0x" + Md5.encode(body.nick).substr(0, 7)).parseInt();
		}

		if (getUserInfos(uid).jsawait() == null) {
			var pi = new PlayerInfo();
			pi.setToDefault();
			pi.pid = uid;
			pi.nick = body.nick;
			writeUserInfos(pi).jsawait();
		}

		var cookie_age = 3600 * 24 * 365 * 5; // 5 years
		res.set({'set-cookie': 'abuid=$uid; Max-Age=$cookie_age'});
		res.set({'content-type': 'text/html'});
		res.redirect("/");
	}

	static function main() {
		var port = 8001;
		var app = new Express();

		app.disable("x-powered-by");

		app.use(new ExpressCookieParser());
		app.use(Express.urlencoded({extended: false}));
		app.get("/logout", logout);
		app.post("/register", registerPlayer);
		app.use("/play/", loadPlayer);
		app.get("/", loadPlayer, (req:ExpressRequest, res:ExpressResponse, next:?Dynamic->Void) -> {
			if (req.locals.uid == null) {
				res.end(Fs.readFileSync('bin/register.html'));
				return;
			}

			if (index_tpl == null) {
				index_tpl = Fs.readFileSync('bin/index.html').toString();
			}

			var player:PlayerInfo = req.locals.player;

			var tpl = index_tpl.replace("{{minerals}}", Std.string(player.minerai))
				.replace("{{playerId}}", player.pid.string());
			res.end(tpl);
		});
		app.use(Express.staticServe('./bin/'));
		app.post('/play/intercom', intercom);

		app.listen(port, function() {
			trace('Listening on port $port');
		});
	}
}
