class TextEs implements mt.Protect {//}

	  /////////////////
	 /// INTERFACE ///
	////////////////

	public static var FUEL_TITLE = "Â¡TANQUE DE COMBUSTIBLE VACIO!";
	public static var FUEL_TEXT = "<p><b>Â¡No te quedan cÃ¡psulas de hidrÃ³geno!</b></p><p>El ESCorp te darÃ¡ tres nuevas cÃ¡psulas <font color='#00FF00'>gratis</font> a medianoche (zona horaria Madrid GMT +1h).</p><p>Sin embargo, todavÃ­a puedes seguir jugando si compras partidas en la Banca Interestelar.</p>";
	public static var FUEL_BANK = "BANCA INTERESTELAR";

	public static var PAUSE_TEXT = [ "SELECCIONA UNA CAPSULA O PULSA LA TECLA \"P\" PARA REGRESAR AL JUEGO","PULSA LA TECLA \"P\" PARA REGRESAR AL JUEGO"];

	public static var WARNING_ZONE = "Â¡CURSOR FUERA DE LA ZONA DE JUEGO!";
	public static var START_CLIC_GREEN = "HAZ CLICK EN LA ZONA VERDE PARA COMENZAR";

	public static var WARNING_FAR = "Â¡Esa posiciÃ³n no puede ser alcanzada en un solo viaje!\nDesplÃ¡zate de casilla en casilla para alcanzar esa posiciÃ³n.";
	public static var WARNING_CARDS = "Â¡Ten cuidado! EstÃ¡s a punto de dejar la zona autorizada.\nTu bola estÃ¡ndar no es lo suficientemente poderosa para ese Ã¡rea.\nDebes obtener la acreditaciÃ³n en primer lugar:";
	public static var ERROR_CRC = "Data reception error. This error can happen if two simultaneous sessions are open in different tabs or browsers.";

	public static var WARNING_CNX = "Â¡Se ha perdido la conexiÃ³n con el servidor!\nEl estatus de la partida no se ha guardado.";

	public static var CONNECTION_SERVER = "CONECTANDO AL SERVIDOR...";

	public static var PREF_FLAGS = ["CONTROL DE TECLADO","MOVIMIENTOS VISIBLES","ZONA DE CONSEJOS","BOLA SUPER CONTRASTADA"];
	public static var PREF_TITLE = "PREFERENCIAS";
	public static var PREF_MOUSE = "SENSIBILIDAD DEL RATON";
	public static var PREF_QUALITY = "CALIDAD GRAFICA";

	public static var CAPS_NAME = ["NADA","HIELO","FUEGO","RAYO"];

	  ////////////
	 /// GAME ///
	////////////

	public static var ITEM_NAMES =		[
		"Primer Nivel",
		"AcreditaciÃ³n Alfa",
		"AcreditaciÃ³n Beta",
		"AcreditaciÃ³n Ceta",
		"Bola perforadora",
		"Llamada de socorro",
		"Douglas",
		"Escombros centrales",
		"Escombros punzantes",
		"Escombros extraÃ±os",
		"Escombros humeantes",
		"Escombros raros",
		"PequeÃ±os escombros",
		"Escombros inofensivos",
		"ExtensiÃ³n de envoltorio",
		"SÃ­mbolos extraÃ±os",
		"Salmeen",
		"---",
		"Misil",

		"Mapa de Mercaderes",
		"Misil Azul",
		"Misil",
		"Piedras Licanas",
		"Piedra Spignysos",
		"Estrella Roja",
		"Estrella Naranja",
		"Estrella Amarilla",
		"Estrella Verde",
		"Estrella Turquesa",
		"Estrella Azul",
		"Estrella Morada",
		"Editor de Minas",
		"MedallÃ³n - parte redonda",
		"MedallÃ³n - parte creciente",
		"MedallÃ³n - parte hueca",
		"MedallÃ³n Zonkeriano",
		"Bola OX-Soldado",
		"Bola OX-Delta",
		"Bola de Asfalto",
		"Misil Rojo",
		"Ambro-X",
		"Radar ok",
		"Generador",

		"NÃºcleo antimateria",
		"NÃºcleo antimateria",
		"NÃºcleo antimateria",
		"NÃºcleo antimateria",

		"Proceso verbal de escape",
		"Escudo atmosfÃ©rico",
		"Armadura externa",
		"Estabilizador hidrÃ¡ulico",
		"Restos de reactor",
		"Superficie de reactor",
		"Traje del espacio",
		"El primo de Salmeen",
		"Chapa del FURI",
		"Pase del CinturÃ³n de Karbonis",

		"ExtensiÃ³n de envoltorio 2",
		"Cristal Rosa A",
		"Cristal Rosa B",
		"Cristal Rosa C",
		"Cristal Rosa D",
		"Cristal Rosa E",
		"Pergamino A",
		"Pergamino B",
		"Pergamino C",
		"Pergamino D",
		"Pergamino E",
		"Pergamino F",
		"Pergamino G",
		"Pergamino H",
		"Accesorio SintrogÃ©nico",
		"ExtensiÃ³n de envoltorio 3",
		"Duplicador de Saumir",
		"Retrofusor de Sactus",

		"Tablilla de Karbonita",
		"Tablilla de Karbonita",
		"Tablilla de Karbonita",
		"Tablilla de Karbonita",
		"Tablilla de Karbonita",
		"Tablilla de Karbonita",
		"Tablilla de Karbonita",
		"Tablilla de Karbonita",
		"Tablilla de Karbonita",
		"Tablilla de Karbonita",
		"Tablilla de Karbonita",
		"Tablilla de Karbonita",

		"Ingeniero de Karbonita",
		"Mina Fora 7R-Z",

		"PID map element 1",
		"PID map element 2",
		"PID map element 3",
		"PID map element 4",
		"PID map element 5",
		"PID map element 6",
		"PID map element 7",
		"PID map element 8",
		"PID map element 9",
		"PID map element 10",
		"PID map element 11",
		"PID map element 12",
		"PID map element 13",
		"PID map element 14",
		"PID map element 15",
		"PID map element 16",
		"PID map element 17",
		"PID map element 18",
		"PID map element 19",
		"PID map element 20",
		"PID map element 21",
		"PID map element 22",
		"PID map element 23",
		"PID map element 24",
		"PID map element 25",
		"PID map element 26",
		"PID map element 27",
		"PID map element 28",
		"PID map element 29",
		"PID map element 30",
		"PID map element 31",
		"PID map element 32",
		"PID map element 33",
		"PID map element 34",
		"PID map element 35",
		"PID map element 36",
		"PID map element 37",
		"PID map element 38",
		"PID map element 39",
		"PID map element 40",
		"PID map element 41",
		"PID map element 42",

		"Earthling passport",
		"Difficult mode",
	];

	public static var SHOP_ITEM_NAMES = [
		"Motor v1",
		"Motor v2",
		"Motor v3",
		"Motor v4",
		"Motor v5",
		"Motor v6",
		"Mapa de Misiles",
		"Gafas de sol",
		"Misil extra nÂº 1",
		"Misil extra nÂº 2",
		"Misil extra nÂº 3",
		"CÃ¡psula de Hielo",
		"CÃ¡psula de Fuego",
		"CÃ¡psula de Agujero Negro",
		"CÃ¡psula SÃ³lida de HidrÃ³geno",
		"Reactores laterales",
		"Refrigerante lÃ­quido",
		"Recarga de municiÃ³n",
		"Envoltorio de seguridad",
		"Robot de apoyo",
		"Robot > Herramientas de PerforaciÃ³n",
		"Robot > Soporte para el motor",
		"Robot > Convertidor",
		"Robot > Colector",
		"Radar de emergencia",
		"CÃ¡psula de rayo",
		"Motor de sÃ­ntesis perpetua",
		"Antena KI-WI",

		"Pata de aterrizaje",
		"ExtensiÃ³n estÃ¡ndar de pata",
		"ExtensiÃ³n especial de pata",
		"ExtensiÃ³n definitiva de pata",
		"Motor turbo de superficie",
		"Motor turbo-X2 de superficie",
		"Motor turbo-X3 de superficie",

		"Mina extra nÂº1",
		"Mina extra nÂº2",
		"Mina extra nÂº3",
	];

	public static var OPTION_NAMES = [
		"ATRACCION",
		"POTENCIA",
		"CONTROL",
		"DISMINUCION",
		"EXTENSION",
		"LLAMA",
		"GLACIAL",
		"HALO",
		"INDIGESTION",
		"JABALINA",
		"KAMIKAZE",
		"LASER",
		"MULTIBOLA",
		"NUEVA BOLA",
		"INTRO",
		"PROVISION",
		"QASAR",
		"REGENERACION",
		"SEGURIDAD",
		"TRANSFORMACION",
		"ULTRAVIOLETA",
		"VOLTIO",
		"WHISKY",
		"XANAX",
		"YO-YO",
		"ZEAL",
		"MISILE",
	];

	  //////////////
	 /// ENDING ///
	//////////////

	public static var OUTRO_0 = "After a few months of travelling through the universe, you are back on Earth.\n\nYour return has pushed the media to force the ESCorp to fulfill its promess towards you.\n\nYou are now free to live as you wish...\n\n\n\n What are you going to do?";

	public static var OUTRO_1 = "After a few months of travelling through the universe, you are back on Earth.\n\nYour revelations about the ESCorp's way of doing things have provoked a scandal throughout the media.\n\nYou are now free to live as you wish...\n\n\n\n What are you going to do?";

	public static var OUTRO_2 = [
		[ 	"Have a peaceful life",
			"The ESCorp will take back your envelope and all your upgrades.\nYour current progression will be lost.",
			"This option allows you to unlock AlphaBounce's <font color='#ff0000'>difficult mode</font>."
		],
		[ 	"Go back in outer space",
			"You will be immediately transferred to your envelope's origin point with all your current upgrades.",
			"Your radar and engine strength will be permanently upgraded by one unit."
		],
	];

	  //////////////
	 /// EDITOR ///
	//////////////

	public static var EDITOR_CLIC_SUPPR = "clic + supprime ; efface une brique.";

	public static var EDITOR_BUTS = [
		"VOLVER",
		"LIMPIAR BLOQUES",
		"GUARDAR",
		"MODERACION",
		"RESETEAR NIVEL",
		"ACEPTAR",
		"NEGAR TODO"
	];

	  ////////////////
	 /// TRAVELER ///
	////////////////

	public static var TRAVELER_NAMES = [ "Walter", "Ben", "Jokarix", "Goshmael", "Mirmonide", "Korkan", "Gifu","Birman","Falgus","Moktin","Bifouak","Lacune","Gibarde","Blafaro","Kimper","Sochmo","Nicolu","Mangerin","Difidus","Stridan","Glochar","Mikou","Kilian","Daston","Possei","Spido","Corneli","Brifuk","Colcanis","Frederak","Coustini","Darnold","Fruncky","Jimic","Sachude","Bramhan","Nucrcela"];

	public static var TRAVELER_JOBS = [
		"fontanero",
		"tÃ©cnico IT",
		"megacrÃ³bata",
		"asesino en serie",
		"::user:: de ::stuff0::",
		"::user:: de ::stuff1::",
		"Agente secreto del ESCorp",
		"Fan de ::singer::",

	];

	public static var TRAVELER_USER = [	"seller",
		"tragador",
		"probador",
		"lanzador",
		"catador",
		"cazador",
	];

	public static var TRAVELER_STUFF_0 = [
		"nata batida",
		"sentimientos",
		"yogur",
		"jabalox",
		"cÃ¡psula de hidrÃ³geno",
		"conglomerados",
		"gemas",
		"plasma energÃ©tico",
		"cables elÃ©ctricos",
		"zapatos lÃ­quidos",
		"crema de carne",
		"restos de envoltorio",
		"kebab",
		"luz cegadora",
		"cartucho de SNES",
		"sushi",
		"croissants",
	];

	public static var TRAVELER_STUFF_1 = [
		"langosta",
		"escalera",
		"espÃ¡rragos",
		"araÃ±as gigantes extraterrestres",
		"pantalla gigante",
		"envoltorio experimental",
		"envoltorio coleccionable",
		"orejas usadas",
		"naranjas",
		"imÃ¡genes coleccionables",
	];

	public static var TRAVELER_MISS = [
		"confianza en sÃ­ mismo",
		"monedas",
		"materia gris",
		"tiempo para el ocio",
		"novias",
		"una forma de hacerme destacar",
		"gente que conozco",
	];


	public static var TRAVELER_SINGER = [
		"Mike Youpala",
		"Brant Burko",
		"Luke Mirno",
		"Costelox",
		"Kirt Siffer",
		"Jess Mystic",
	];

	public static var TRAVELER_INTRO = [
		"Hola prisionero humano,\n",
		"Hola,\n",
		"Querido desconocido,\n",
		"Bienvenido extraÃ±o,\n",
		"He estado esperando tu visita desde hace tiempo...\n",
		"Por fin, Â¡un visitante!\n",
		"Â¡Umph!\n",
		"Â¡Hola! Â¿Hay alguien por ahÃ­?\n",
		"Â¿QuiÃ©n anda ahÃ­?\n",
		"Â¡Hola!\n",
	];

	public static var TRAVELER_WHO = [
		"Mi nombre es ::name::, soy ::profession:: en este planeta.",
		"Mi nombre es ::name::, podemos ser amigos si asÃ­ lo deseas.",
		"Soy un humilde ::profession::.",
		"Me llaman ::name:: el ::profession::.",
		"Mi nombre es ::name::, Â¿quieres ser mi amigo?",
		"Mi nombre es ::name::, no tengo muchos ::miss:: desde que soy un ::profession::.",
		"Â¿QuiÃ©n eres? Soy ::name:: el ::profession::.",
	];

	public static var TRAVELER_LEAVE = [
		"Desde siempre he querido abandonar ::start::.",
		"::start:: no me gusta tanto como antes,",
		"::start:: no es un buen lugar para vivir,",
		"No hay nada que hacer en ::start::,",
		"Â¿Crees que podrÃ­as vivir en ::start::? Porque yo personalmente ya he tenido suficiente...",
		"No hay mucha gente por aquÃ­... si esto sigue asÃ­ perderÃ© mi trabajo."
	];


	public static var TRAVELER_DEST = [
		"SÃ©, desde lo mÃ¡s profundo de mÃ­, que una vida mejor me espera en ::end::.",
		"QuizÃ¡s podrÃ­a empezar desde cero en ::end::.",
		"Siempre he soÃ±ado con ir a ::end::",
		"Mi sueÃ±o es ir a ::end::",
	];

	public static var TRAVELER_DEST_COORD = [
		"Necesito encontrar mi keychain... Lo perdÃ­ mientras viajaba por ::pos::.",
		"Mi proyecto es construir una nueva colonia en ::pos::.",
		"Me gustarÃ­a abrir una nueva tienda espacial en ::pos::.",
		"Si pudieras llevarme a ::pos::, podrÃ­a encontrarme con mi tÃ­o, quien lleva una hamburgueserÃ­a-satÃ©lite por la zona.",
		"He oÃ­do hablar de una discoteca astral muy chula en ::pos::.",
		"Las cajas de ::stuff0:: han sido abandonadas en ::pos::, Â¡me gustarÃ­a ir para allÃ¡ lo mÃ¡s rÃ¡pido posible!",
		"Alguien me dijo que ::singer:: va a dar un concierto sorpresa en ::pos:: esta semana.",
	];

	public static var TRAVELER_ASK_0 = [
		"Â¿Me podrÃ­as llevar allÃ­?",
		"Â¿Me ayudarÃ­as a viajar hasta allÃ­?",
		"Â¿PodÃ­a acompaÃ±arte hasta allÃ­?",
		"Â¿PodrÃ­a ir contigo?",
	];

	public static var TRAVELER_ASK_1 = [
		"TendrÃ© que usar uno de tus envoltorios para poder viajar contigo.",
		"Necesito uno de tus envoltorios de emergencia para seguirte, soy demasiado grande para estar en un solo envoltorio contigo.",
	];

	public static var TRAVELER_REWARD_MIN_0 = [
		"Si me llevas te puedo dar ::rmin:: minerales.",
		"Puedo pagarte con ::rmin:: minerales.",
	];

	public static var TRAVELER_REWARD_MIN_1 = [
		"Â¡Son todos mis ahorros!",
		"No me queda nada mÃ¡s.",
		"No tienes por quÃ© cogerlo todo...",
		"Espero que sea suficiente.",
	];

	public static var TRAVELER_REWARD_KEUD = [
		"",
		"No tengo dinero para pagarte el viaje, pero estoy seguro que me ayudarÃ¡s con todo tu corazÃ³n...",
	];

	public static var TRAVELER_REWARD_CAPS = [
		"TambiÃ©n tengo algo para echarte una mano con el combustible: Â¡::rcap:: CSH!",
		"Para el combustible tambiÃ©n tengo ::rcap:: CSH, las cuales deben ayudarte para viajar un poco.",
	];

	public static var TRAVELER_NO_SLOT = "\nPero no puedes hacer nada para mÃ­...\nGracias por tu visita de todos modos. Sienta bien charlar con alguien de vez en cuando.";

	public static var TRAVELER_LEAVE_PLANET = [
		[	// 0 - MOLTEAR
			"Las molÃ©culas espaciales hacen nuestra vida imposible... Ayer bloquearon la puerta de mi salÃ³n.",
			"Las molÃ©culas se estÃ¡n multiplicando muy rÃ¡pidamente por esta zona, creo que es momento de partir.",
			"Â¡Es muy molesto! Las molÃ©culas destruyeron mi ::stuff0:: otra vez esta maÃ±ana. Ya no me quedan mÃ¡s razones para vivir aquÃ­.",
		],
		[	// 1 - SOUPALINE
			"El aire marino de Supaline nunca me hizo nada bueno. Creo que la sal empezÃ³ a erosionar mi cerebro.",

		],
		[	// 2 - LYCANS
			"::start:: es demasiado inestable para mÃ­, Â¡ayer el repartidor de ::stuff0:: fue propulsado hasta la Ã³rbita tras una explosiÃ³n en la superficie!",
			"Â¿Crees que podrÃ­as vivir en ::start::? Â¡Pff, allÃ­ hay al menos 20 explosiones cada noche!",
			"He perdido 13 Shmolgs desde que empezara la guerra... Y todo esto a causa de las explosiones de sulfuro en ::start::.",
		],
		[""],	// 3 - SAMOSA
		[	// 4 - TIBOON
			"Arena, arena y mÃ¡s arena. No hay nada mÃ¡s aquÃ­...",
			"He explorado todas la dunas de ::start::. Creo que es hora de hacer otra cosa.",
		],
		[	// 5 - BALIXT
			"Los balixteanos son gente opresora y rencorosa. Â¡La situaciÃ³n aquÃ­ es horrible!",
			"Ayer Franxis fue golpeado por uno de esos asquerosas seres, Â¡y desde entonces no he podido encontrarlo!",
			"El nuevo gobernador de Balixt hace que la vida de los extraÃ±os sea muy difÃ­cil.",
		],
		[""],	// 6 - KARBONIS
		[	// 7 - SPIGNYSOS
			"::start::, digamos que estÃ¡ un poco muerto en la Ã©poca de invierno, sabes lo que quiero decir...",
			"Â¿Has visto el tiempo tan malo que hace? De ninguna manera, Â¡no pienso quedarme mÃ¡s en ::start::!",
			"Anoche la temperatura descendiÃ³ hasta -50ÃÂ°C, he perdido un dedo del pie...",
			"Â¡Mi ::stuff0:: se congelÃ³ anoche!",
		],
		[	// 8 - POFIAK
			"::start:: es demasiado hÃºmedo para mÃ­, seguro que acabarÃ© enfermando si me quedo aquÃ­.",
			"Los ataques de los insectos psionicos me han convencido para abandonar ::start::.",

		],
		[""],	// 9 - SENEGARDE
		[	// 10 - DOURIV
			"EstÃ¡n viniendo muchos mineros, Â¡pronto ::start:: estarÃ¡ cubierto de complejos mineros autÃ³nomos!",

		],
		[""],	// 11 - GRIMORN
		[	// 12 - DTRITUS
			"La calidad del aire del planeta se ha deteriorado y finalmente comer niÃ±os no es lo mÃ­o...",
		],
		[ 	// 13 - ASTEROBELT
			"La vida de un ermitaÃ±o perdido en un asteroide se estÃ¡ volviendo muy aburrida.",
		],
		[	// 14 - NALIKORS
			"Cada dÃ­a hay mÃ¡s RAIDs del ESCorp, creo que mi vida corre peligro aquÃ­.",
			"Vine para unirme a los FURI pero la actitud un tanto manÃ­aca de Kefrid me hace dudar...",
		],
		[	// 15 - HOLOVAN
			"ComencÃ© mi meditaciÃ³n trascendental interna con los kemilianos hace unos 37 aÃ±os.",
			"Desde que terminara mis estudios, ya nada me ata a Holovan.",
		],
		[	// 16 - Khorlan
			"Quiero viajar a travÃ©s del universo, Â¡igual que Salmeen!",
			"Las nueces que estaban en la Ã³rbita han caÃ­do sobre mi pueblo y lo han destrozado, Â¡solo queda en pie mi casa! Â¡No quiero quedarme aquÃ­ solo!",
		],
		[	// 17 - CILORILE
			"A causa de los atascos no podemos movernos entre las 9h y 9h20 por la maÃ±ana y las 18h y las 18:50 por la tarde. Esto no es vida, Â¡quiero marcharme de este planeta!"
		],
		[""],	// 18 - TARCITURNE
		[""],	// 19 - CHAGARINA
	];

	public static var TRAVELER_DEST_PLANET = [
		[	// 0 - MOLTEAR
			"Las molÃ©culas espaciales parecen muy interesantes, creo que podrÃ© estudiarlas cuando llegue allÃ­."
		],
		[	// 1 - SOUPALINE
			"Tener el ocÃ©ano a la vista me hace soÃ±ar despierto..."
		],
		[	// 2 - LYCANS
			"Espacios abiertos... Â¡no hay nada mejor!"
		],
		[""],	// 3 - SAMOSA
		[	// 4 - TIBOON
			"En cualquier sitio estarÃ© mÃ¡s tranquilo que en este pequeÃ±o y ruidoso planeta."
		],
		[	// 5 - BALIXT
			"Los balixteanos necesitan mano de obra para construir su imperio. Seguramente necesitarÃ¡n algunos ::profession:: por aquÃ­.",
			"Las instalaciones de Reducine necesitan mucha mano de obra. Seguro que encuentras un trabajo aquÃ­."
		],
		[""],	// 6 - KARBONIS
		[	// 7 - SPIGNYSOS
			"Estamos sofocados aquÃ­, Â¡necesito aire fresco!",
			"Â¡Dicen que la superficie es tan brillante que apenas se pueden abrir los ojos!"
		],
		[	// 8 - POFIAK
			"Necesito algo de vegetaciÃ³n."

		],
		[""],	// 9 - SENEGARDE
		[	// 10 - DOURIV
			"Â¡Dicen que por allÃ­ todo lo que tienes que hacer es agacharte para coger los cristales! Â¿No te parece increÃ­ble?",
			"PodrÃ­a ser rico allÃ­, parece que la superficie estÃ¡ cubierta de cristales."
		],
		[""],	// 11 - GRIMORN
		[	// 12 - DTRITUS
			"Â¡He oÃ­do que allÃ­ se puede empezar una carrera profesional de asustador de niÃ±os!"
		],
		[ 	// 13 - ASTEROBELT
			""
		],
		[	// 14 - NALIKORS
			"Â¡Unirse a los FURI para buscar aventura debe ser toda una experiencia!"
		],
		[	// 15 - HOLOVAN
			"Mi sueÃ±o es encontrar los kemilianos y vivir con ellos."
		],
		[	// 16 - KHORLAN
			"Me gustarÃ­a un poco de vegetaciÃ³n."
		],
		[	// 17 - CILORILE
			"Marine de aire: Â¡bien estÃ¡ para lo que tengo!"
		],
		[""],	// 18 - TARCITURNE
		[""],	// 19 - CHAGARINA
	];


	  //////////////////
	 /// ITEM GIVER ///
	//////////////////

	public static var ITEM_GIVER_SALMEEN_COUSIN = "Â¡Hola Salmeen!\nHacÃ­a tiempo que no nos veÃ­amos! Â¿Necesitas algo? Mmm.... vienes con un amigo: verÃ© si puedo encontrar lo que estÃ¡s buscando.\n*Gregune abre un gran cofre en la parte trasera de la habitaciÃ³n*\nAquÃ­ tienes un traje espacial suptirneano, lo que quiere decir que tiene un par de mangas en la parte trasera que no te serÃ¡n muy Ãºtiles, pero bueno, de algo te servirÃ¡. EstÃ¡ equipado con un jetpack que te permitirÃ¡ moverte con facilidad. Suerte para los dos, Â¡hasta pronto!";

	public static var ITEM_GIVER_BADGE_FURI = "Â¡Bienvenido compaÃ±ero!\nEl RCEH necesita cualquier alma disponible para luchar contra la expansiÃ³n de la humanidad. No tenemos polÃ­ticas discriminatorias y tu orÃ­genes humanos no son un obstÃ¡culo para unirse a nuestra causa. Puedes participar en las operaciones de sabotaje del ESCorp para el rescate de prisioneros en este sistema.\nÂ¡Gracias por tu ayuda!";

	public static var ITEM_GIVER_SAUMIR = "Â¡ExtraÃ±o noyaguldo! Soy Saumir.\nLos kemilianos estÃ¡n felices de poder darte la bienvenida. Nuestra gente vino a Holovan hace miles de aÃ±os, no deseamos participar en los conflictos Ã©tnicos de tu joven civilizaciÃ³n. TodavÃ­a tenÃ©is que progresar en la historia para poder llegar a comprender el gran destino de Koshmerate.\nQue el Gran Kluc estÃ© contigo extraÃ±o. LlÃ©vate esta bola duplicadora, te servirÃ¡ de gran ayuda.";

	public static var ITEM_GIVER_SACTUS = "Hola prisionero.\nSoy el doctor Sactus, pero puedes llamarme Doc. Ãste es mi laboratorio, donde construyo todo con los recursos metÃ¡licos de Grimorn. El Retrofusor estÃ¡ aquÃ­ listo, puedes llevÃ¡rtelo. Cuando lo uses, asegÃºrate de no pulsar el botÃ³n del ratÃ³n con el dedo anular, o acabarÃ¡s teleportado al centro de la supernova de Zambreze, donde tu masa molecular serÃ­a terriblemente influida.\nÂ¡Espero que hayas prestado atenciÃ³n a mis instrucciones! Â¡Nos vemos!";
	public static var ITEM_GIVER_SAFORI_0 = "Mi nombre es Safori. Vine a Nalikors cuando mi planeta de origen, Karbonis, explotÃ³.\nAhora estoy atascado en este planeta. Me gustarÃ­a trabajar otra vez: Soy un experto arquiniero. Puedo reconstruir cualquier mÃ¡quina vieja si tengo los planos y el material necesario... Desafortunadamente, no hay ningÃºn proyecto disponible por aquÃ­.\nGracias por tu visita, Â¡ya nos veremos!";

	public static var ITEM_GIVER_SAFORI_1 = "Â¡Â¡Estupendo!!! Â¡Gracias a estas tablillas podrÃ© finalmente trabajar! Veamos... hmmmm parece interesante, es como un sistema antiguo de navegaciÃ³n. Tengo todo lo necesario para construirlo. Espera un momento, por favor. \n............\n............\n............\n............\n............\n\nÂ¡AhÃ­ tienes!\nÂ¡Para ti! Gracias a este nuevo sistema de navegaciÃ³n, el rango del radar de tu envoltorio espacial ha sido mejorado.\nGracias por las tablillas, Â¡las guardarÃ©!!\nQue las puertas de Shamu estÃ©n abiertas para ti, amigo mÃ­o.";

	public static var ITEM_GIVER_COMBINAISON = "Hola prisionero, hermo preparado tu traje espacial. Por favor rellena el formulario DZ-578 y deja tus huellas dactilares en las zonas A, B y C de estos papeles.\n...\nGracias\n...\nAquÃ­ tienes tu traje espacial.\nBuena suerte.";

	public static var ITEM_GIVER_TABLET_KARBONIS_0 = "La Memoria de Karbonis ";
	public static var ITEM_GIVER_TABLET_KARBONIS_1 = [
		"corre en tus venas",
		"estÃ¡ en cada uno de nosotros",
		"no puede desaparecer",
		"brilla en tus ojos",
		"es un tesoro",
		"serÃ¡ salvada",
		"es el mÃ¡s preciado de los tesoros de Zonker",
		"estÃ¡ escrita en el corazÃ³n de Shamu",
		"no debe caer en las manos equivocadas",
		"estÃ¡ preservada en este lugar",
		"estÃ¡ escondida en cada uno de estos asteroides",
		"viaja a travÃ©s del tiempo y del espacio",
	];
	public static var ITEM_GIVER_TABLET_KARBONIS_2 = "...\nCoge esta tablilla y protÃ©gela.";
	public static var ITEM_GIVER_TABLET_KARBONIS_3 = "AquÃ­ no eres bienvenido.";

	public static var ITEM_GIVER_EMAP_0 = "Bienvenu terrien. Je pense que cette artefact ancien issu de ta civilisation pourrait t'aider a retrouver ta route. Je peux te l'Ã©changer contre ::price:: minerais qu'en penses-tu ?  ";

	public static var BUTTON_PEOPLE = [
		"ACCEPTER",
		"REFUSER",
		"PARTIR",
		"DENONCER",
	];

	  ///////////////////
	 /// FURI MEMBER ///
	///////////////////

	public static var FURI_HELLO = [
		"Â¡Hola amigo!\n",
		"Â¡Hola compaÃ±ero!\n",
		"Â¡Hola!\n",
		"Bienvenido a mi hogar\n",
	];

	public static var FURI_ARGUE = [
		"Â¿Sabes cuÃ¡ntos planetas estÃ¡n ocupados o minados por los humanos en el universo? Unos 35 millones. El 30% de estos planetas estÃ¡n operados por el ESCorp, el cual se acerca cada vez mÃ¡s a nuestro sistema.\n",
		"Desde que el ESCorp empezara a enviar prisioneros a nuestro sistema, algunos planetas como Tiboon o Licanos han sido devastados. Sus peligrosos experimentos han incluso provocado la explosiÃ³n de Karbonis, un planeta en plena expansiÃ³n.\n",
		"Desde su llegada, la desatenciÃ³n a las leyes naturales ha provocado enormes catÃ¡strofes: la explosiÃ³n de Karbonis, el descenso de la natalidad de los Glurt de Moltear, la contaminaciÃ³n del ocÃ©ano de Supaline, etc. Una autÃ©ntica tragedia.\n",
		"El ESCorp empezÃ³ sus operaciones mineras y carcelarias en este sistema hace 30 aÃ±os. Desde su llegada, su indiferencia hacia la naturaleza provocÃ³ enormes catÃ¡strofes. Los vientos solares son cada vez mÃ¡s cÃ¡lidos, los Glurt de Moltear sufren un descenso de la natalidad... todas estas tragedias se deben a la expansiÃ³n del ser humano.\n"
	];

	public static var FURI_REWARD_MIN = "Â¡Puedo ayudarte, compaÃ±ero! LlÃ©vate estos :::rmin: minerales y Ãºsalos con sabidurÃ­a.\nEs todo lo que me queda.";
	public static var FURI_REWARD_CAPS = "Tengo algo que puede ayudarte en tu aventura, Â¡amigo mÃ­o! LlÃ©vate estas ::rcaps:: CSH.\nGracias, Â¡nuestro vuelo va a partir!";

	public static var FURI_END_0 = "La FundaciÃ³n de Unidad Racionalista de la Infinidad (FURI), ";
	public static var FURI_END_1 = [
				"sugiere alternativas para permitir a las gentes del universo usar los recursos sin daÃ±ar la biodiversidad de nuestro universo estelar.",
				"actÃºa en contra de la expansiÃ³n incontrolada organizando sabotajes con el objetivo de reducir las grandes corporaciones humanas como el ESCorp",
			];

	public static var FURI_BETRAY = [
		"Â¡Ayuda!",
		"Has tomado una decisiÃ³n muy triste, amigo.",
		"Â¿Por quÃ© tanto odio?",
		"Supongo que el ESCorp te paga bien por este trabajo...",
	];

	public static var FURI_LUCK = [
		"Â¡Buena suerte!",
		"Cumple tu destino, humano. Â¡Salva nuestro universo!",
		"Â¡Que las puertas de Shamu estÃ©n abiertas para ti!",
	];

	//////////////
	/// GOSSIP ///
	//////////////

	public static var GOSSIP_CRYSTAL = "En un vuelo rutinario de hiperespacio vi unos extraÃ±os resplandores rosas en ::coord::. Es muy raro encontrar eso a tal velocidad. Debe de haber algo interesante por allÃ­.";


	public static var GOSSIP_NOYAUX_0 = [
		"Un equipo entero de hierro-criquet",
		"La cÃ¡psula del espacio de mi tÃ­o",
		"Una flota balixteana completa",
		"Un escuadrÃ³n de 4 envoltorios de prisioneros del ESCorp",
		"Una ballena estelar de unas 650 toneladas de peso",
	];

	public static var GOSSIP_NOYAUX_1 = " ha sido misteriosamente tragado por un punto negro en el espacio. El equipo de rescate de mi pueblo se pasÃ³ mÃ¡s de una semana en el Ã¡rea ::coord::, pero no encontrÃ³ nada.";

	public static var GOSSIP_TABLET = "SobrevivÃ­ a la explosiÃ³n de Karboni, pero toda mi familia pereciÃ³. Los restos de nuestra civilizacÃ³n flota en el espacio... *snif*. Mientras exploraba el cinturÃ³n de asteroides encontrÃ© una tablilla de karbonita en ::coord::. No pude llevarla conmigo porque era demasiado pesada pra mÃ­.";

	public static var GOSSIP_ASPHALT = "Se dice que el ESCorp estÃ¡ trabajando en una superpotente bola perforadora en el sistema Stuklie, al sudeste de nuesta posiciÃ³n.\nTienen que mandarnos los resultados muy pronto.";

	public static var GOSSIP_DEFAULT = [
		"El FURI ha organizado varias revueltas. El aÃ±o pasado una delegaciÃ³n de unos 350 representantes consiguieron obtener una reuniÃ³n con el presidente de la confederaciÃ³n humana en la Tierra.",
		"Nunca volverÃ© a ver a mis amigos de Karbonis. DÃ©jame en paz. Vosotros los humanos no sabÃ©is apreciar lo que de verdad tiene valor.",
		"Nebulae brilla tanto que a menudo molesta a los pilotos. Por ello nuestros pilotos nunca salen sin sus gafas de sol.",
		"Odio la compota.",
		"En el universo hay algunos conglomerados tan potentes que pueden invertir tus sentidos.",
		"El aÃ±o pasado mis vacaciones en Samosa fueron horribles. Â¡No tuvimos buen tiempo en toda una semana!",
		"Los conejo-robots nunca pudieron entrar en nuestro sistema. Creo que la gente debe quejarse menos del ESCorp. Â¡Desde que los humanos llegaron aquÃ­ no ha habido ni una sola guerra mÃ¡s!",
		"Una patrulla de conejo-robots se llevÃ³ a mi abuela hace 28 aÃ±os y nunca mÃ¡s la hemos vuelto a ver... Es muy difÃ­cil de decir, pero debo admitir que la presencia del ESCorp ha contribuido a la seguridad de nuestro sistema.",
	];

	public static var GOSSIP_MISSILE_0 = "Mientras estaba dando una vuelta en el Ã¡rea ::coord:: vi una carcasa de un misil viejo.\n";
	public static var GOSSIP_MISSILE_1 = [
		"El espacio se ha vuelto un lugar muy sucio.",
		"Los jÃ³venes no tienen respeto por nada...",
		"Espero que los coleccionistas de restos pudieran cogerlo.",
		"No me dejÃ© llevar por el miedo a una explosiÃ³n.",
		"Estaba en muy mal estado.",
	];

}
