import haxe.ds.IntMap;
import haxe.ds.StringMap;
import js.Browser.document;
import mt.bumdum.Lib;
import Protocol;

using Lambda;

class Web {
	static public function checkTasks() {
		// checks mission popups
		if (Cs.pi.tasks.length > 0) createMissionEndNotification(Cs.pi.tasks.pop());
	}

	static public function createMissionEndNotification(data:_LogData) {
		document.getElementById("inner").innerHTML = createMissionHtml(data);
		document.getElementById("pop").style.display = "block";
	}

	static public function createMissionList() {
		var list:Array<_LogData> = [];
		var pi = Cs.pi;
		
		// compile list with data
		for (i in 0...MissionInfo.LIST.length) if (pi.missions[i].status == 0 && MissionInfo.LIST[i].desc != null) {
			var m:_LogData = {
				_id: i,
				_status: 0,
				_timestamp: pi.missions[i].timestamp,
			}
			list.push(m);
		}
		for (p in pi.travel) {
			var m:_LogData = {
				_id: 1002,
				_status: 0,
				_timestamp: p._timestamp,
				_data: [
					"tname" => p._name,
					"x" => Std.string(p._ex),
					"y" => Std.string(p._ey),
				],
			};
			list.push(m);
		}
		
		if (list.length > 1) list.sort((a, b) -> Std.int(b._timestamp - a._timestamp));
		
		// add level mission at the top
		var str = "";
		if (pi.levelMission != null) {
			var m:_LogData = {
				_id: pi.faction == 1 ? 1000 : 1001,
				_status: 0,
				_timestamp: pi.levelMission.timestamp,
				_data: [
					"reward" => Std.string(pi.faction == 1 ? (pi.levelMission.size * pi.levelMission.size * 10) : 75),
					"xmin" => Std.string(pi.levelMission.x),
					"ymin" => Std.string(pi.levelMission.y),
				],
			};
			if (pi.faction == 1) {
				m._data.set("xmax", Std.string(pi.levelMission.x + pi.levelMission.size - 1));
				m._data.set("ymax", Std.string(pi.levelMission.y + pi.levelMission.size - 1));
			}
			str += createMissionHtml(m);
		}
		for (m in list) str += createMissionHtml(m);
		document.getElementById("section").innerHTML = str;
		
		checkTasks();
	}

	static public function createMissionHtml(data:_LogData) {
		var name = Texts.texts.get('mission_name_${data._id}');
		var date = DateTools.format(Date.fromTime(data._timestamp), Texts.texts.get("date_fmt"));
		var desc = Texts.texts.get(data._status == 1 ?  'mission_end_${data._id}' : 'mission_desc_${data._id}');
		desc = Str.searchAndReplace(desc, "::name::", Cs.pi.nick);
		if (data._data != null) {
			for (key in data._data.keys()) {
				desc = Str.searchAndReplace(desc, '::$key::', data._data.get(key));
			}
		}
		return '<div class="${data._status == 1 ? "KMissionEnd" : "KMissionStart"}"><div class="mission"><div class="date">$date</div><h3>$name</h3><div class="content"><div class="pic"><img src="/img/vigs/vig${data._id}.png" alt="Img"></div> $desc</div></div></div>';
	}

	static public function updateMinerals() {
		document.getElementById("mineral").innerHTML = Std.string(Cs.pi.minerai);
	}
}
