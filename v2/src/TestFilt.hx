import haxe.Timer;
import pixi.core.text.TextStyle;
import pixi.core.math.Point;
import js.html.CanvasElement;
import pixi.core.math.shapes.Rectangle;
import pixi.filters.extras.GlowFilter;
import mt.DepthManager;
import pixi.core.math.Matrix;
import mt.bumdum.Lib.Num;
import navi.menu.Shop.PixiGlowFilter;
import mt.bumdum.Lib.Filt;
import pixi.core.graphics.Graphics;
import pixi.core.utils.Utils;

private typedef Planete = {gri:Array<Array<Int>>, pop:Float, hor:Int, gMax:Int, dMax:Int, hc:Float, min:Array<Int>, type:Int, g:Float};

class TestFilt {
	public static var WIDTH = 1600;
	public static var HEIGHT = 700;

	public static var GMIN = 60;

	// DM
	public static var DP_BG = 0;
	public static var DP_PLAN = 1; // > B DM
	public static var DP_INTER = 2;

	// B DM
	public static var DP_UNDERPARTS = 0;
	public static var DP_PAD = 1;
	public static var DP_GROUND = 2; // > G DM
	public static var DP_HERO = 3;
	public static var DP_FOREGROUND = 4;
	public static var DP_DRONES = 5;
	public static var DP_PARTS = 6;

	// G DM
	public static var DP_DECOR = 1;
	public static var DP_MINERALS = 2;
	public static var DP_FRONT = 4;

	static public var flMarkHouse:Bool;
	static public var flHouseVisited:Bool;
	static public var col:Int;

	static public var fadeCoef:Float;

	static public var chs:Int;
	static public var item:Int;
	static public var debit:Int;

	static public var pl:Planete;
	static public var seed:mt.OldRandom;

	static function main() {
		Utils.skipHello();

		var view:CanvasElement = cast js.Browser.document.getElementById("main");
		view.width = WIDTH;
		view.height = HEIGHT;
		var app = new pixi.core.Application({width: WIDTH, height: HEIGHT, view: view});

		/*seed = new mt.OldRandom(50 * 10000 + 27);

			PixelHelper.app = app;

			pl = lander.Game.PLANETES[7];

			// INIT
			var root = new ASprite();
			app.stage.addChild(root);

			BmpTextureHelper.preload(['groundText']).then((_) -> {
				var dm = new DepthManager(root);
				// dm.attachBitmap(bmpGround, 0);

				drawGround(dm);
		});*/

		var style = new TextStyle({
			fill: "#26a269",
			fontFamily: "Verdana",
			fontSize: 11,
			trim: true,
			wordWrap: true,
			wordWrapWidth: 360
		});

		Timer.delay(() -> {
			var text = new pixi.core.text.Text("Bonjour l'ami !
	Depuis que l'ESCorp a commencé a envoyer ses détenus dans ce système, plusieurs planètes comme Tiboon ou Lycans ont été ravagées. Leur experiences dangeureuses ont même provoqué l'explosion de Karbonis, une planète autrefois florissante.
	La Fondation pour l'Unification Rationelle de l'Infinie (F.U.R.I), s'oppose activement a une expansion humaine incontrolée en organisant des operations de sabotages destinées a affaiblir les grandes corporations humaine telle que l'ESCorp",
				style);

			text.x = 20;
			text.y = 20;

			app.stage.addChild(text);
		}, 300);
	}

	static public function drawGround(root:DepthManager) {
		// SHAPE
		var shape = RenderTexture.create(WIDTH, pl.gMax);
		var mcShape = new Graphics();
		mcShape.beginFill(0xFF0000, 100);
		var gy = pl.gMax - GMIN;
		var x = 0.0;
		var y = gy * 0.5;

		mcShape.moveTo(0, y);
		while (true) {
			x = Math.min(x + 50 + seed.rand() * 200, WIDTH);
			y = Num.mm(25, y + (seed.rand() * 2 - 1) * 40, gy);
			mcShape.lineTo(x, y);
			if (x == WIDTH)
				break;
		}
		mcShape.lineTo(WIDTH, pl.gMax);
		mcShape.lineTo(0, pl.gMax);
		mcShape.lineTo(0, 0);
		mcShape.endFill();
		shape.draw(mcShape, new Matrix());
		mcShape.destroy();

		// TEXT
		var side = 100;
		var text = RenderTexture.create(side, side);
		var mcText = BmpTextureHelper.getASprite("groundText");
		mcText.anchor.set(0, 0);
		mcText.x = -340;
		mcText.gotoAndStop(7 + 1);
		text.draw(mcText, new Matrix());
		mcText.destroy();

		var textPx = text.extract();
		var shapePx = shape.extract();

		var bmpGroundPx = RenderTexture.create(WIDTH, HEIGHT).extract();

		// APPLY
		var xmax = Std.int(WIDTH / side) + 1;
		var ymax = Std.int(pl.gMax / side) + 1;
		for (x in 0...xmax) {
			for (y in 0...ymax) {
				var p = new pixi.core.math.Point(x * side, y * side);
				bmpGroundPx.copyPixels(textPx, new Rectangle(0, 0, side, side), p, shapePx, p, true);
			}
		}
		text.destroy();
		shape.destroy();

		// SOL
		var list = pl.gri;
		for (a in list) {
			var fl = new GlowFilter(a[0], a[1], 0, a[2], 1);
			// bmpGround.applyFilter(bmpGround, bmpGround.rectangle, new pixi.core.math.Point(0, 0), fl);
			trace("Filter");
		}

		var texture = untyped (Texture.fromBuffer)(bmpGroundPx.getPixels(), WIDTH, HEIGHT);
		root.attachBitmap(texture, 0);
	}
}
