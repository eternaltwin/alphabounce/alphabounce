import js.Browser;
import haxe.Json;
import MissionInfo;
import Protocol;
import mt.OldRandom;

class PlayerInfo { // }
	static public var FL_RESET = true;

	public var flAdmin:Bool;
	public var flEditor:Bool;

	public var nick:String;
	public var pid:Int;

	public var ox:Int;
	public var oy:Int;

	public var x:Int;
	public var y:Int;

	public var chl:Int;
	public var chs:Int;
	public var minerai:Int;
	public var missions:Array<{status:Int, timestamp:Float}>;
	public var missionLog:Array<_LogData>;
	public var levelMission:Null<{x:Int, y:Int, size:Int, timestamp:Float}>; // clean-up/liberation mission
	public var rank:Int; // completed clean-up/liberation missions
	public var faction:Int; // 0 => fugitive; 1 => ESCorp; 2 => FURI
	public var missile:Int;
	public var missileMax:Int;
	public var plays:Int;

	var life:Int;

	public var engine:Int;
	public var radar:Int;
	public var drone:Int;

	public var pendingLevels:Int;

	public var fog:Array<Int>;
	public var items:Array<Int>;
	public var shopItems:Array<Int>;
	public var comp:Array<Int>;

	public var travel:Array<_Travel>;
	public var houseDone:Array<Array<Int>>;
	
	public var tasks:Array<_LogData>;

	public static var DEFAULT_X = 0;
	public static var DEFAULT_Y = 0;

	public function new() {}

	public function exportInfos() {
		var pc = new mt.PersistCodec();
		pc.crc = true;

		var outObj = {
			_flAdmin: flAdmin,
			_flEditor: flEditor,
			_nick: nick,
			_pid: pid,
			_x: x,
			_y: y,
			_ox: ox,
			_oy: oy,
			_chl: chl,
			_chs: chs,
			_minerai: minerai,
			_missions: missions,
			_missionLog: missionLog,
			_missile: missile,
			_missileMax: missileMax,
			_plays: plays,
			_engine: engine,
			_radar: radar,
			_life: life,
			_drone: drone,
			_pendingLevels: pendingLevels,
			_levelMission: levelMission,
			_rank: rank,
			_faction: faction,
			_items: items,
			_shopItems: shopItems,
			_fog: fog,
			_comp: comp,
			_travel: travel,
			_tasks: tasks,
		};

		return pc.encode(outObj);
	}

	public function parseInfo(str) {
		var pc = new mt.PersistCodec();
		pc.crc = true;
		var info:_PlayerData = pc.decode(str);

		flAdmin = info._flAdmin;

		flEditor = info._flEditor;

		nick = info._nick;
		pid = info._pid;
		x = info._x;
		y = info._y;
		ox = info._ox;
		oy = info._oy;

		chl = info._chl;
		chs = info._chs;
		minerai = info._minerai;
		missions = info._missions;
		missionLog = info._missionLog;
		missile = info._missile;
		missileMax = info._missileMax;
		plays = info._plays;

		engine = info._engine;
		radar = info._radar;
		life = info._life;
		drone = info._drone;
		pendingLevels = info._pendingLevels;
		levelMission = info._levelMission;
		rank = info._rank;
		faction = info._faction;

		items = info._items;
		shopItems = info._shopItems;
		fog = info._fog;
		comp = info._comp;

		if (fog.length == 0 && items[0] == MissionInfo.COLLECTED_INV) {
			var size = 20 * 20;
			for (_x in 0...size) {
				for (_y in 0...size) {
					var realX = x + _x - 10;
					var realY = y + _y - 10;
					if (realX == 0 && realY == 0)
						fog.push(1);
					else
						fog.push(null);
				}
			}
		}

		travel = info._travel;
		tasks = info._tasks;
	}

	public function setToDefault() {
		/*
			Cs.log("---------");
			Cs.log("RESET ALL");
			Cs.log("---------");
		 */

		flAdmin = false;
		flEditor = false;

		nick = null;
		pid = Std.random(200);
		x = DEFAULT_X;
		y = DEFAULT_Y;
		ox = DEFAULT_X;
		oy = DEFAULT_Y;

		chl = 3;
		chs = 12;
		minerai = 0;
		missions = [for (i in MissionInfo.LIST) {status: -1, timestamp: -1}];
		missionLog = [];
		missile = 0;
		missileMax = 0;
		plays = 0;
		levelMission = null;
		rank = 0;
		faction = 1;

		engine = 1;
		radar = 1;
		life = 3;
		drone = 0;
		pendingLevels = -1;

		items = [MissionInfo.TRIGGER];
		shopItems = [];
		comp = [];
		fog = [];

		travel = [];
		houseDone = [];
		tasks = [];

		checkMission();
	}

	// TOOLS
	public function getLife() {
		return life - travel.length;
	}

	public function getRay() {
		var sr = 32;
		#if test
		return 32;
		#end
		if (gotItem(MissionInfo.EXTENSION))
			sr += 10;
		if (gotItem(MissionInfo.EXTENSION_2))
			sr += 8;
		if (gotItem(MissionInfo.EXTENSION_3))
			sr += 10;
		if (gotItem(MissionInfo.EVASION))
			sr -= 10;

		return sr;
	}

	public function getMissileType() {
		var n = 0;
		if (gotItem(MissionInfo.MISSILE_BLUE))
			n = 1;
		if (gotItem(MissionInfo.MISSILE_BLACK))
			n = 2;
		if (gotItem(MissionInfo.MISSILE_RED))
			n = 3;
		return n;
	}

	public function getMissileCadence() {
		var n = 16;
		if (shopItems[ShopInfo.COOLER] == 1)
			n = 8;
		return n;
	}

	public function getMissileTurnSpeed() {
		var n = 0.2;
		if (shopItems[ShopInfo.LATERAL] == 1)
			n = 0.35;
		return n;
	}

	// IS ?
	public function gotItem(n) {
		var n = items[n];
		return n == MissionInfo.COLLECTED || n == MissionInfo.COLLECTED_INV;
	}

	public function loadCache() {
		var so = JSSharedObject.getLocal("info");
		if (so.data.x == null || FL_RESET || so.data.missions.length == 0) {
			saveCache();
			return;
		}

		flAdmin = true;
		flEditor = false;

		nick = null;
		pid = so.data.pid;
		x = so.data.x;
		y = so.data.y;
		ox = x;
		oy = y;

		chl = so.data.chl;
		chs = so.data.chs;
		minerai = so.data.minerai;
		missions = so.data.missions;
		missionLog = so.data.missionLog;

		missile = so.data.missile;
		missileMax = so.data.missileMax;

		engine = so.data.engine;
		radar = so.data.radar;
		life = so.data.life;
		drone = so.data.drone;
		levelMission = so.data.levelMission;
		rank = so.data.rank;
		faction = so.data.faction;

		items = so.data.items;
		shopItems = so.data.shopItems;
		comp = so.data.comp;

		travel = so.data.travel;
		houseDone = so.data.houseDone;
		tasks = [];

		// shopItems[ShopInfo.SUNGLASSES] = null ;
		// items[MissionInfo.MODE_DIF] = 2 ;

		items[MissionInfo.MINES] = null;

		/*
			if(flAdmin){
				for( i in 0...130) items[i] = 2;
				for( i in 0...50) shopItems[i] = 1;
				missile = 50;
				missileMax = 50;


			}
			// */
		/*
			items[MissionInfo.MINES] = 	2;
			shopItems[ShopInfo.MINE_0] = 	0;
			shopItems[ShopInfo.MINE_1] = 	0;
			shopItems[ShopInfo.MINE_2] = 	0;
			/ */
		/*
			var so = JSSharedObject.getLocal("pendingLevels");
			var px = x+Api.CACHE_GRID_MARGIN;
			var py = y+Api.CACHE_GRID_MARGIN;
			pendingLevels = so.data.grid.get('${px},${py}').length;
			if( pendingLevels == 0 ){
				var so2 = JSSharedObject.getLocal("baseNiveaux");
				if( so2.data.grid.get('${px},${py}')!=null ){
					pendingLevels = -1;
				}
			}
		 */
		/* HACK
			shopItems[ShopInfo.SUNGLASSES] = 	1;
			shopItems[ShopInfo.PODS] = 		1;

			shopItems[ShopInfo.LANDER_REACTOR_0] =	0;

			shopItems[ShopInfo.PODS_EXTEND_0] =	0;
			shopItems[ShopInfo.PODS_EXTEND_1] =	0;
			shopItems[ShopInfo.PODS_EXTEND_2] =	0;


			items[MissionInfo.LANDER_REACTOR] = 	2;


			items[MissionInfo.EXTENSION] = 		2;
			items[MissionInfo.COMBINAISON] = 	2;
			items[MissionInfo.MISSILE_BLACK] = 	2;
			// */
	}

	public function saveCache(?so:Dynamic) {
		if (so == null)
			so = JSSharedObject.getLocal("info");

		// trace(saveCache)

		// if(minerai<=0)minerai=1000;
		// shopItems=[];*
		// items[MissionInfo.MAP_SHOP] = MissionInfo.COLLECTED_INV;
		// Cs.log("save--->"+items);

		so.data.flAdmin = flAdmin;
		so.data.flEditor = flEditor;

		so.data.pid = pid;
		so.data.x = x;
		so.data.y = y;

		so.data.chl = chl;
		so.data.chs = chs;
		so.data.minerai = minerai;

		so.data.missions = missions;
		so.data.missionLog = missionLog;
		so.data.missile = missile;
		so.data.missileMax = missileMax;

		so.data.engine = engine;
		so.data.radar = radar;
		so.data.life = life;
		so.data.drone = drone;
		so.data.levelMission = levelMission;
		so.data.rank = rank;
		so.data.faction = faction;

		so.data.items = items;
		so.data.shopItems = shopItems;
		so.data.comp = comp;

		so.data.travel = travel;
		so.data.houseDone = houseDone;

		// trace( (so.data.x==null)+" || "+(FL_RESET)+" || "+(so.data.missions.length==0) );

		so.flush();

		// trace( (so.data.x==null)+" || "+(FL_RESET)+" || "+(so.data.missions.length==0) );
	}

	// EMULATION SERVEUR
	// - Fonctions destinées au devellopement, a réécrire coté serveur.
	// Ajoute un item au joueur
	public function addItem(id) {
		if (id == null)
			return;

		// VALIDE L'ITEM
		var n = items[id];
		if (n == MissionInfo.TRIGGER) {
			items[id] = MissionInfo.COLLECTED_INV;
		} else {
			items[id] = MissionInfo.COLLECTED;
		}

		// MISSILES
		if (id >= MissionInfo.MISSILE && id < MissionInfo.MISSILE + MissionInfo.MISSILE_MAX) {
			missileMax++;
			missile = missileMax;
		}

		//
		switch (id) {
			case MissionInfo.SUPER_RADAR:
				radar++;
		}
	}

	// Achete un shopItem.
	public function buyShopItem(id) {
		var price = getPrice(id);
		// ADD ITEM
		switch (id) {
			case ShopInfo.CHS:
				chs++;
			case ShopInfo.AMMO:
				missile = missileMax;
			// navi.Map.me.game.missile.set(new mt.flash.VarSecure(missileMax));
			default:
				shopItems[id] = 1;
				if (id == ShopInfo.LIFE)
					life++;
				if (id == ShopInfo.DRONE)
					drone++;
				if (id == ShopInfo.RADAR) {
					radar++;
					addItem(MissionInfo.RADAR_OK);
				}
				if (id >= ShopInfo.ENGINE && id < ShopInfo.ENGINE + ShopInfo.ENGINE_MAX) {
					var pw = 2 + id - ShopInfo.ENGINE;
					if (pw > engine)
						engine = pw;
				}
				if (id >= ShopInfo.MISSILE && id < ShopInfo.MISSILE + 3) {
					missileMax++;
					missile = missileMax;
				}
		}

		// APPLY PRICE
		minerai -= price;
	}

	// Calcul le prix d'un élément en boutique
	public function getPrice(id) {
		/*
			var item = ShopInfo.ITEMS[id];
			var p = item.price;

			switch(id){
				case ShopInfo.AMMO: 	p = missileMax-missile;A
				case ShopInfo.DRONE: 	p =  Std.int( Math.pow(drone+6,4)/100 )*100;
			}

			var seed = new mt.OldRandom( x*10000 + y + id*20 );
			var sens = seed.random(2)*2-1;
			var c = Math.pow(seed.rand(),4);
			p += Std.int(p*0.5*c*sens);
		 */

		var p = switch (id) {
			case ShopInfo.AMMO: missileMax - missile;
			case ShopInfo.DRONE: Std.int(Math.pow(drone + 6, 4) / 100) * 100;
			default: ShopInfo.ITEMS[id].price;
		}

		var seed = new mt.OldRandom(Std.int(Math.abs(x) * 10000) + Std.int(Math.abs(y) + id * 20));
		var sens = seed.random(2) * 2 - 1;
		var c = Math.pow(seed.rand(), 4);
		p += Std.int(p * 0.5 * c * sens);

		return p;
	}

	// Vérifie les missions - a appeler apres chaque endGame
	public function checkMission() {
		var changed = false;

		// CHECK FINISH
		for (n in 0...MissionInfo.LIST.length) {
			var mi = MissionInfo.LIST[n];
			if (missions[n].status == 0 && isAllConditionsOk(mi.conditions)) {
				changed = true;
				missions[n].status = 1;
				missions[n].timestamp = -1;
				trace('Set mission $n to 1');
				var t:_LogData = {
					_id: n,
					_status: 1,
					_timestamp: Date.now().getTime(),
				}
				tasks.push(t);
				missionLog.push(t);
				for (a in mi.endItem)
					addStuff(a);
			}
		}

		// CHECK NEW
		for (n in 0...MissionInfo.LIST.length) {
			var mi = MissionInfo.LIST[n];
			if (missions[n].status == -1 && isAllConditionsOk(mi.startConditions)) {
				missions[n].status = 0;
				missions[n].timestamp = Date.now().getTime();
				changed = true;
				trace("new[" + n + "]");
				if (MissionInfo.LIST[n].desc != null) {
					var t:_LogData = {
						_id: n,
						_status: 0,
						_timestamp: missions[n].timestamp,
					}
					missionLog.push(t);
				}
				for (a in mi.startItem)
					addStuff(a);
			}
		}

		// trace(missions);
		return changed;
	}

	// Vérifie si les conditions sont remplies
	public function isAllConditionsOk(list:Array<Array<Int>>) {
		for (a in list) {
			if (!isConditionOk(a))
				return false;
		}
		return true;
	}

	public function isConditionOk(a) {
		switch (a[0]) {
			case MissionInfo.GOT_ITEM:
				for (n in 1...a.length)
					if (!gotItem(a[n]))
						return false;

			case MissionInfo.NOT_ITEM:
				return !gotItem(a[1]);

			case MissionInfo.GOT_SHOPITEM:
				for (n in 1...a.length)
					if (shopItems[a[n]] != 1)
						return false;

			case MissionInfo.GOT_PLANET:
				return comp[a[1]] >= 100;

			case MissionInfo.GOT_MINERAI:
				return a[1] <= minerai;

			case MissionInfo.ENTER_ZONE:
				var dx = a[1] - x;
				var dy = a[2] - y;
				var dist = Math.sqrt(dx * dx + dy * dy);
				return dist <= a[3];

			case MissionInfo.LEAVE_ZONE:
				var dx = a[1] - x;
				var dy = a[2] - y;
				var dist = Math.sqrt(dx * dx + dy * dy);
				return dist > a[3];

			case MissionInfo.GOT_MISSION:
				var m = missions[a[1]];
				if (m == null) return false;
				return missions[a[1]].status == 1;

			case MissionInfo.IS_LEVEL:
				return rank >= a[1];
		}
		return true;
	}

	// ajoute au joueur les recompenses d'une mission
	public function addStuff(a) {
		switch (a[0]) {
			case MissionInfo.MINERAI:
				minerai += a[1];
			case MissionInfo.CHS:
				trace("mission reward: CHS are not in use");
			case MissionInfo.NEW_LIFE:
				life++;
			case MissionInfo.LOSS_LIFE:
				life--;
			case MissionInfo.NEW_RADAR:
				radar++;
			case MissionInfo.LOSS_RADAR:
				radar--;
			case MissionInfo.RESET_MISSION:
				missions[a[1]].status = -1;
				missions[a[1]].timestamp = -1;
			case MissionInfo.ENABLE_LEVEL_MISSIONS:
				faction = 1;
				levelMission = null;
			case MissionInfo.ENABLE_REBEL_MISSIONS:
				faction = 2;
				levelMission = null;
			case MissionInfo.DISABLE_LEVEL_MISSIONS:
				faction = 0;
				levelMission = null;
			default:
				items[a[0]] = a[1];
				trace('set item ${a[0]}: ${a[1]}');
		}
	}
	// {
}
/*
	Je vais apporter quelques précisions pour qu'on se comprenne mieux :

	Le mode nightmare est un mode difficile.
	Par difficile j'entend que même les joueurs les plus expérimentés auront besoins de plusieurs tentative pour finir certains niveaux.
	C'est un mecanisme de jeu-video plutot standard : je rate -> je recommence.

	Le problème en ce qui concerne alphabounce est que le jeu de départ ne respecte pas réellement ce principe afin d'etre accessible au plus grand nombre et ne pas





 */
