class ShopInfo { // }
	//  SHOP ITEM
	public inline static var ENGINE_MAX = 6;

	static var maxItem = 0;
	public inline static var ENGINE = 0;

	public inline static var MISSILE_MAP = 6;
	public inline static var SUNGLASSES = 7;
	public inline static var MISSILE = 8;

	public inline static var ICE = 11;
	public inline static var FIRE = 12;
	public inline static var BLACKHOLE = 13;
	public inline static var CHS = 14;
	public inline static var LATERAL = 15;
	public inline static var COOLER = 16;
	public inline static var AMMO = 17;
	public inline static var LIFE = 18;
	public inline static var DRONE = 19;
	public inline static var DRONE_PERFO = 20;
	public inline static var DRONE_SPEED = 21;
	public inline static var DRONE_CONVERTER = 22;
	public inline static var DRONE_COLLECTOR = 23;
	public inline static var RADAR = 24;
	public inline static var STORM = 25;
	public inline static var MISSILE_GENERATOR = 26;
	public inline static var ANTENNA = 27;

	public inline static var PODS = 28;
	public inline static var PODS_EXTEND_0 = 29;
	public inline static var PODS_EXTEND_1 = 30;
	public inline static var PODS_EXTEND_2 = 31;
	public inline static var LANDER_REACTOR_0 = 32;
	public inline static var LANDER_REACTOR_1 = 33;
	public inline static var LANDER_REACTOR_2 = 34;

	public inline static var MINE_0 = 35;
	public inline static var MINE_1 = 36;
	public inline static var MINE_2 = 37;

	public static var ITEMS = [
		{pb: [1], price: 20}, // Moteur v1,
		{pb: [2], price: 200}, // Moteur v2,
		{pb: [3, 8], price: 1000}, // Moteur v3,
		{pb: [4, 14], price: 8000}, // Moteur v4,
		{pb: [7, 20], price: 20000}, // Moteur v5,
		{pb: [6, 60], price: 160000}, // Moteur v6,
		{pb: [2, 60], price: 2000}, // Carte des missiles,
		{pb: [6, 20], price: 200}, // Lunettes de soleil,
		{pb: [1], price: 100}, // Missile sup no1,
		{pb: [10], price: 1000}, // Missile sup no2,
		{pb: [50], price: 10000}, // Missile sup no3,
		{pb: [4, 6], price: 30}, // Capsule glace,
		{pb: [2], price: 20}, // Capsule feu,
		{pb: [7, 14], price: 50}, // Capsule trou noir,
		{pb: [1], price: 210}, // Capsule hydrogène solide,		// 90
		{pb: [8, 12], price: 240}, // Réacteurs latéraux,
		{pb: [7, 20], price: 570}, // Reffroidissement liquide,
		{pb: [1], price: 5}, // Recharge munitions,
		{pb: [10], price: 700}, // Envellope de secours,
		{pb: [2, 10], price: 0}, // Drone de soutient,
		{pb: [7, 20], price: 5500}, // Drone > Outils de perforation
		{pb: [12, 20], price: 1250}, // Drone > Support réacteur,
		{pb: [16, 20], price: 11500}, // Drone > Convertisseur,
		{pb: [1, 20], price: 2500}, // Drone > Collecteur,
		{pb: [1], price: 60}, // Radar de secours,
		{pb: [3, 6], price: 30}, // Capsule Foudre,
		{pb: [3, 70], price: 16000}, // Moteur de synthèse perpetuel,
		{pb: [2, 110], price: 4850}, // Antenne KI-WI,
		{pb: [1], price: 0}, // Pod d'atterrisage,
		{pb: [8, 40], price: 3500}, // extension de pod standard,
		{pb: [11, 100], price: 12000}, // extension de pod speciale,
		{pb: [10, 150], price: 75000}, // extension de pod ultime,
		{pb: [3, 30], price: 7500}, // reacteur de surface turbo,
		{pb: [11, 100], price: 30000}, // reacteur de surface turbo-X2,
		{pb: [10, 300], price: 240000}, // reacteur de surface turbo-X3,
		{pb: [3, 150], price: 15200}, // mine supplementaire,
		{pb: [5, 200], price: 36400}, // mine supplementaire,
		{pb: [8, 250], price: 88000}, // mine supplementaire,
	];
	// {
}
