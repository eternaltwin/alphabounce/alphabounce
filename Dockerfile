FROM debian:bookworm

RUN echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections

RUN apt-get update && apt-get -y upgrade
RUN apt-get update && apt-get install -y supervisor

ADD ./scripts/start.sh /start.sh
ADD ./configs/supervisord.conf /etc/supervisord.conf
RUN chmod 755 /start.sh

RUN DEBIAN_FRONTEND=noninteractive apt-get -y install curl
RUN curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.40.0/install.sh | bash
RUN /bin/bash -c '. /root/.nvm/nvm.sh && nvm install v20'

RUN mkdir /root/alphabounce/
RUN mkdir /var/log/alphabounce/
RUN bash -c '. /root/.nvm/nvm.sh'
ADD ./configs/*.conf /etc/supervisor/conf.d/
ADD ./v2/api-bin/package.json /root/alphabounce/
ADD ./v2/api-bin/package-lock.json /root/alphabounce/
RUN bash -c '. /root/.nvm/nvm.sh && cd /root/alphabounce/ && npm i'

EXPOSE 3000
COPY ./v2/api-bin/ /root/alphabounce/
COPY ./v2/bin/ /root/alphabounce/bin/

CMD ["/bin/bash", "/start.sh"]
